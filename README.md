# M2SysDemo
This is a demo project for HRM management.
It includes:
	*Employee
	*Leave
Technology Used:
	1. Back End
		a) FrameWork:
			i)ASP.NET Core 3.1
		b) Backend language:
			i)C#
		c) Entity FrameWork Core (Database first)
	2. Frontend:
		a)	Angular 8
		b)	TypeScript 3.5.3
		c)	Bootstrap 4.3.1
		d)	sweetalert2 10.6.1
Important Notes:
	1. Execute "Db Scripts.sql" for database table and sp creation
Database Scaffolding:
	1. Please set M2SysDemo.HRMS.Api as startup project.
	2. Then run this command in package manager console
			Scaffold-DbContext -Connection Name=M2SysDemoDB Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models -Project "M2SysDemo.DAL" -force
Run Project:
	Please go to solution properties.
	Then select multiple startup project.
	Then select M2SysDemo.Web and M2SysDemo.HRMS.Api as startup project
	


