IF ( NOT EXISTS (SELECT name 
FROM master.dbo.sysdatabases 
WHERE name = 'M2SysDemoDb'))
Begin 
	Create Database M2SysDemoDb
End
ELSE
Begin
	Select 'Database already exist'
End
go

use M2SysDemoDb
go
IF NOT EXISTS (SELECT * 
           FROM INFORMATION_SCHEMA.TABLES 
           WHERE 
		   TABLE_CATALOG='M2SysDemoDb'
		   AND TABLE_TYPE='BASE TABLE' 
		   AND TABLE_SCHEMA='dbo'
           AND TABLE_NAME='Department'
		   ) 
Begin
	Create table Department
	(
		Id int primary key not null Identity(1,1),
		Name nvarchar(50) not null
	)
End

go

IF NOT EXISTS (SELECT * 
           FROM INFORMATION_SCHEMA.TABLES 
           WHERE 
		   TABLE_CATALOG='M2SysDemoDb'
		   AND TABLE_TYPE='BASE TABLE' 
		   AND TABLE_SCHEMA='dbo'
           AND TABLE_NAME='Designation'
		   ) 
Begin
	Create table Designation
	(
		Id int primary key not null Identity(1,1),
		Name nvarchar(50) not null
	)
End

go

IF NOT EXISTS (SELECT * 
           FROM INFORMATION_SCHEMA.TABLES 
           WHERE 
		   TABLE_CATALOG='M2SysDemoDb'
		   AND TABLE_TYPE='BASE TABLE' 
		   AND TABLE_SCHEMA='dbo'
           AND TABLE_NAME='Employee'
		   ) 
Begin
	Create table Employee
	(
		Id int primary key not null Identity(1,1),
		FirstName nvarchar(50) not null,
		MiddleName nvarchar(50),
		LastName nvarchar(50),
		DateOfBirth datetime not null,
		JoiningDate datetime not null,
		DesignationId int not null foreign key REFERENCES Designation(Id),
		DepartmentId int not null foreign key REFERENCES Department(Id),
	)
End
go
IF NOT EXISTS (SELECT * 
           FROM INFORMATION_SCHEMA.TABLES 
           WHERE 
		   TABLE_CATALOG='M2SysDemoDb'
		   AND TABLE_TYPE='BASE TABLE' 
		   AND TABLE_SCHEMA='dbo'
           AND TABLE_NAME='Leave'
		   ) 
Begin
	Create table Leave
	(
		Id int primary key not null Identity(1,1),
		EmployeeId int not null foreign key REFERENCES Employee(Id),
		LeaveType int not null,
		Description nvarchar(200),
		StartDate datetime not null,
		EndDate datetime not null
	)
End

go

IF NOT EXISTS (SELECT * 
           FROM INFORMATION_SCHEMA.TABLES 
           WHERE 
		   TABLE_CATALOG='M2SysDemoDb'
		   AND TABLE_TYPE='BASE TABLE' 
		   AND TABLE_SCHEMA='dbo'
           AND TABLE_NAME='OrganizationalConfig'
		   ) 
Begin
	Create table OrganizationalConfig
	(
		Id int primary key not null Identity(1,1),
		Type nvarchar(50) not null,
		Name nvarchar(50) not null,
		Value nvarchar(50) not null,
		Status bit not null,
		[Order] int not null
	)
End
go

---Sample data Designation---
go

Insert into dbo.Designation Values ('Managing Director')
go
Insert into dbo.Designation Values ('Head Of IT')
go
Insert into dbo.Designation Values ('Project Manager')
go
Insert into dbo.Designation Values ('Lead Software Engineer')
go
Insert into dbo.Designation Values ('Senior Software Engineer')
go
Insert into dbo.Designation Values ('Software Engineer')
go
Insert into dbo.Designation Values ('Assistant Software Engineer')
go

---Sample data Department---
go

Insert into dbo.Department values ('HRM')
go
Insert into dbo.Department values ('IT')
go
Insert into dbo.Department values ('Account')
go

---Sample data Employee---
DELETE FROM dbo.Employee
DBCC CHECKIDENT ('M2SysDemoDb.dbo.Employee',RESEED, 0)
go

Insert into dbo.Employee (FirstName,MiddleName,LastName,DateOfBirth,JoiningDate,DesignationId,DepartmentId) Values ('Shovon','Bisshash','Shuvro','01-01-1993','01-01-2018',2,2)
go
Insert into dbo.Employee (FirstName,MiddleName,LastName,DateOfBirth,JoiningDate,DesignationId,DepartmentId) Values ('Nishat','','Khan','01-01-1993','01-01-2018',3,2)
go
Insert into dbo.Employee (FirstName,MiddleName,LastName,DateOfBirth,JoiningDate,DesignationId,DepartmentId) Values ('Shourov','','Bisshash','01-01-1993','01-01-2018',4,2)
go
Insert into dbo.Employee (FirstName,MiddleName,LastName,DateOfBirth,JoiningDate,DesignationId,DepartmentId) Values ('Abbas','','Ahmed','01-01-1993','01-01-2018',5,2)
go
Insert into dbo.Employee (FirstName,MiddleName,LastName,DateOfBirth,JoiningDate,DesignationId,DepartmentId) Values ('Abir','','Khan','01-01-1993','01-01-2018',5,2)
go

---Sample data OrganizationalConfig---
truncate table dbo.OrganizationalConfig
go

Insert into dbo.OrganizationalConfig (Type,Name,Value,Status,[Order]) Values ('LeaveType','Sick leave',1,1,1)
go
Insert into dbo.OrganizationalConfig (Type,Name,Value,Status,[Order]) Values ('LeaveType','Casual leave',2,1,2)
go
Insert into dbo.OrganizationalConfig (Type,Name,Value,Status,[Order]) Values ('LeaveType','Maternity leave',3,1,3)
go
Insert into dbo.OrganizationalConfig (Type,Name,Value,Status,[Order]) Values ('LeaveType','Unpaid Leave',4,1,4)
go



--Get Employee List sp--
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.GetEmployeeList'))
Begin
	drop procedure dbo.GetEmployeeList
End

Go

Create procedure dbo.GetEmployeeList
(
	@searchParam nvarchar(100),
	@pageNumber int,
	@rowsPerPage int

)
--exec dbo.GetEmployeeList '',1,2
--exec dbo.GetEmployeeList 'Head',1,2
As Begin
	IF(@pageNumber<1)
		Set @pageNumber=1;

	IF((Select LEN(LTRIM(RTRIM(@searchParam))))=0)
	Begin 
		Select emp.*
		from dbo.Employee emp
		order by emp.DesignationId,emp.JoiningDate desc
		OFFSET (@pageNumber-1)*@rowsPerPage ROWS
		FETCH NEXT @rowsPerPage ROWS ONLY
	End
	Else
	Begin
		Select emp.*
		from dbo.Employee emp
		inner join dbo.Department dept on dept.Id=emp.DepartmentId
		inner join dbo.Designation desg on desg.Id=emp.DesignationId
		where
		(LEN(LTRIM(RTRIM(@searchParam)))<>0 
		or(emp.FirstName like '%'+@searchParam+'%'
		or emp.MiddleName like '%'+@searchParam+'%'
		or emp.LastName like '%'+@searchParam+'%'
		or cast (emp.DateOfBirth as nvarchar) like '%'+@searchParam+'%'
		or cast(emp.JoiningDate as nvarchar) like '%'+@searchParam+'%'
		or dept.Name like '%'+@searchParam+'%'
		or desg.Name like '%'+@searchParam+'%'))
		
		order by emp.DesignationId,emp.JoiningDate desc
		OFFSET (@pageNumber-1)*@rowsPerPage ROWS
		FETCH NEXT @rowsPerPage ROWS ONLY
	End
End
go

--Get Leave List sp--
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND OBJECT_ID = OBJECT_ID('dbo.GetEmployeeLeaveList'))
Begin
	drop procedure dbo.GetEmployeeLeaveList
End

Go

Create procedure dbo.GetEmployeeLeaveList
(
	@employeeId int,
	@searchParam nvarchar(100),
	@pageNumber int,
	@rowsPerPage int

)
--exec dbo.GetEmployeeLeaveList 6,'',1,2
--exec dbo.GetEmployeeLeaveList 1,'abc',1,2
As Begin
	IF(@pageNumber<1)
		Set @pageNumber=1;

	Select leave.* from dbo.Leave leave
	inner join (
		Select * from dbo.OrganizationalConfig where Type='LeaveType'
	) leaveType on leave.LeaveType=leaveType.Value
	where
	leave.EmployeeId=@employeeId
	and( LEN(LTRIM(RTRIM(@searchParam)))<>0 or(leave.Description like '%'+@searchParam+'%'
	or cast (leave.StartDate as nvarchar) like '%'+@searchParam+'%'
	or cast(leave.EndDate as nvarchar) like '%'+@searchParam+'%'
	or leaveType.Name like '%'+@searchParam+'%'))	 
	order by leave.StartDate desc

	OFFSET (@pageNumber-1)*@rowsPerPage ROWS
	FETCH NEXT @rowsPerPage ROWS ONLY
End
go