﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M2SysDemo.AppModel
{
    public partial class CrudResult
    {
        public CrudResult()
        {

        }
        public CrudResult(bool success, string message)
        {
            Success = success;
            Message = message;
        }
        public string Message { get; set; }
        public bool Success { get; set; }
        public long Id { get; set; }
        public dynamic Entity { get; set; }
    }
}
