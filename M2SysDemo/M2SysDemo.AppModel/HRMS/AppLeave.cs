﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M2SysDemo.AppModel.HRMS
{
    public partial class AppLeave
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int LeaveType { get; set; }
        public string LeaveTypeName { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
