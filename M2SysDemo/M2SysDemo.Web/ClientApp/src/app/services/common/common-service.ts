import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  entity: any={};
  genericAddConfirmationMessage: string;
  genericEditConfirmationMessage: string;
  genericDeleteConfirmationMessage: string;
  genericAddErrorMessage: string;
  genericEditErrorMessage: string;
  genericDeleteErrorMessage: string;
  genericAddSuccessfulMessage: string;
  genericEditSuccessfulMessage: string;
  genericDeleteSuccessfulMessage: string;
  genericAddConfirmButtonYesText: string;
  genericAddConfirmButtonNoText: string;
  genericEditConfirmButtonYesText: string;
  genericEditConfirmButtonNoText: string;
  genericDeleteConfirmButtonYesText: string;
  genericDeleteConfirmButtonNoText: string;

  constructor() {
    this.genericAddConfirmationMessage = "### will be added, are you sure?";
    this.genericEditConfirmationMessage = "### will be edited, are you sure?";
    this.genericDeleteConfirmationMessage = "### will be deleted, are you sure?";

    this.genericAddErrorMessage = "Error while adding ###.";
    this.genericEditErrorMessage = "Error while editing ###.";
    this.genericDeleteErrorMessage = "Error while deleting ###.";

    this.genericAddSuccessfulMessage = "### added successfully.";
    this.genericEditSuccessfulMessage = "### edited successfully.";
    this.genericDeleteSuccessfulMessage = "### deleted successfully.";

    this.genericAddConfirmButtonYesText = "Yes, add it!";
    this.genericAddConfirmButtonNoText = "No";

    this.genericEditConfirmButtonYesText = "Yes, edit it!";
    this.genericEditConfirmButtonNoText = "No";

    this.genericDeleteConfirmButtonYesText = "Yes, delete it!";
    this.genericDeleteConfirmButtonNoText = "No";

    this.entity.Employee = "Employee";
    this.entity.Leave = "Leave";
  }
  replaceMessage(message, entity) {
    return message.replace(/###/g, entity);
  }
}
