import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  constructor(private http: HttpClient) { }
  getFilterData() {
    return this.http.get(environment.apiEndPoint + "employee/filterdata");
  }
  getEmployeeFilter() {
    return this.http.get(environment.apiEndPoint + "employee/employeefilter");    
  }
  getEmployee(employeeId: number) {
    return this.http.get(environment.apiEndPoint + "employee/" + employeeId);
  }
  getEmployees(pageNumber: number, rowsNumberPerPage: number, searchParam: string) {
    return this.http.get(environment.apiEndPoint + "employee/" + pageNumber + "/" + rowsNumberPerPage + "/" + searchParam);
  }
  getTotalEmployeeCount(searchParam: string) {
    return this.http.get(environment.apiEndPoint + "employee/employeecount/"+ searchParam);
  }
  addEmployee(employee) {
    return this.http.post(environment.apiEndPoint + "employee/", employee);
  }
  editEmployee(employee) {
    return this.http.put(environment.apiEndPoint + "employee/", employee);
  }
  deleteEmployee(id) {
    return this.http.delete(environment.apiEndPoint + "employee/" + id);
  }
}

