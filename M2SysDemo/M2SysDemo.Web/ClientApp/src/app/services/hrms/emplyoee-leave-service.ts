import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class EmployeeLeaveService {
  constructor(private http: HttpClient) { }

  getFilterData() {
    return this.http.get(environment.apiEndPoint + "leave/filterdata");
  }
  getEmployeeLeaveCount(employeeId: number, searchParam: string) {
    return this.http.get(environment.apiEndPoint + "leave/employeeleavecount/" + employeeId + "/" + searchParam);
  }
  getLeave(id: number) {
    return this.http.get(environment.apiEndPoint + "leave/" + id);
  }
  getLeavesByEmployee(employeeId: number, pageNumber: number, rowsNumberPerPage: number, searchParam: string) {
    return this.http.get(environment.apiEndPoint + "leave/employeeleave/" + employeeId + "/" + pageNumber + "/" + rowsNumberPerPage + "/" + searchParam);
  }
  addEmployeeLeave(data) {
    return this.http.post(environment.apiEndPoint + "leave/", data);
  }
  editEmployeeLeave(data) {
    return this.http.put(environment.apiEndPoint + "leave/", data);
  }
  deleteEmployeeLeave(id: number) {
    return this.http.delete(environment.apiEndPoint + "leave/" + id);
  }
}
