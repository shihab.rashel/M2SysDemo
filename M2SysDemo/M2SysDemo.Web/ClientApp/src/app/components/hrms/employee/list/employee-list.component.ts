import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Employee } from "../../../../models/hrms/employee.model";
import { EmployeeService } from "../../../../services/hrms/employee-service";
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { CommonService } from "../../../../services/common/common-service";


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})

export class EmployeeListComponet implements OnInit {
  searchParam: string ;
  rowNumberListToShow: Array<number>;
  rowNumberToShow: number;
  pageNumber: number;
  totalEmployeeCount: number;
  totalPage: number;

  employees: Array<Employee> = [];
  ngOnInit(): void {
    this.getTotalEmployeeCount();
  }

  constructor(private employeeService: EmployeeService,
    private router: Router,
    private commonService: CommonService) {
    this.searchParam = "";
    this.rowNumberListToShow = [];

    this.rowNumberListToShow.push(2);
    this.rowNumberListToShow.push(10);
    this.rowNumberListToShow.push(20);
    this.rowNumberListToShow.push(50);

    this.rowNumberToShow = this.rowNumberListToShow[0];

    this.pageNumber = 1;
    this.totalEmployeeCount = 0;
    this.totalPage = 0;
  }

  navigateAddEmployee() {
    this.router.navigate(['employee-add']);
  }

  navigateEditEmployee(empId) {
    this.router.navigate(['employee-edit'], { queryParams: { e: empId, v: false } });
  }

  viewEmployee(empId) {
    this.router.navigate(['employee-edit'], { queryParams: { e: empId,v:true } });
  }

  getEmployees() {
    this.employeeService.getEmployees(this.pageNumber, this.rowNumberToShow, this.searchParam).subscribe((res: any) => {
      this.employees = res;
      console.log(this.employees);
    });
  }

  getTotalEmployeeCount() {
    this.pageNumber = 1;
    this.employees = [];
    this.employeeService.getTotalEmployeeCount(this.searchParam).subscribe((res: any) => {
      this.totalEmployeeCount = res;
      this.totalPage = Math.ceil(this.totalEmployeeCount / this.rowNumberToShow);
      this.getEmployees();
    });
  }
  onPreviousClick() {
    if (this.pageNumber > 1)
      this.pageNumber -= 1;
    this.getEmployees();
  }
  onNextClick() {
    this.pageNumber++;
    if (this.pageNumber > this.totalPage)
      this.pageNumber = this.totalPage;
    this.getEmployees();
  }

  deleteEmployee(id) {
    Swal.fire({
      title: this.commonService.replaceMessage(this.commonService.genericDeleteConfirmationMessage, this.commonService.entity.Employee),
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: this.commonService.genericDeleteConfirmButtonYesText,
      cancelButtonText: this.commonService.genericDeleteConfirmButtonNoText
    }).then((result) => {
      if (result.value) {
        this.employeeService.deleteEmployee(id).subscribe((res: any) => {
          if (res.Success) {
            Swal.fire('Success', this.commonService.replaceMessage(this.commonService.genericDeleteSuccessfulMessage, this.commonService.entity.Employee), 'success');
            this.getTotalEmployeeCount();
          } else {
            Swal.fire('Error', this.commonService.replaceMessage(this.commonService.genericDeleteErrorMessage, this.commonService.entity.Employee), 'error');
          }
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          '',
          'error'
        );
      }
    });
  }
}
