import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Employee } from "../../../../models/hrms/employee.model";
import { M2SysIntFilter } from "../../../../models/m2sys-int-filter";
import { EmployeeService } from "../../../../services/hrms/employee-service";
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { CommonService } from "../../../../services/common/common-service";

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})
export class EmployeeAddComponent implements OnInit {
  employee: Employee;
  departmentList: Array<M2SysIntFilter>;
  designationList: Array<M2SysIntFilter>;
  formValidation: any;
  ngOnInit(): void {
    this.getFilterData();
  }
  constructor(private employeeService: EmployeeService,
    private router: Router,
    private commonService: CommonService) {
    this.employee = new Employee();
    this.departmentList = [];
    this.designationList = [];
    this.formValidation = { FirstName: '', Designation: '', Department: '', DateOfBirth: '', JoiningDate: '' };
  }
  getFilterData() {
    this.employeeService.getFilterData().subscribe((res: any) => {
      if (res != null) {
        if (res.Departments != null) {
          this.departmentList = res.Departments;
          this.employee.DepartmentId = this.departmentList[0].Id;
        }

        if (res.Designations != null) {
          this.designationList = res.Designations;
          this.employee.DesignationId = this.designationList[0].Id;
        }
      }
    });
  }
  addEmployee() {
    if (this.isValidForm()) {
      this.employee.DepartmentId = parseInt(this.employee.DepartmentId.toString());
      this.employee.DesignationId = parseInt(this.employee.DesignationId.toString());
      Swal.fire({
        title: this.commonService.replaceMessage(this.commonService.genericAddConfirmationMessage, this.commonService.entity.Employee),
        text: '',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: this.commonService.genericAddConfirmButtonYesText,
        cancelButtonText: this.commonService.genericAddConfirmButtonNoText
      }).then((result) => {
        if (result.value) {
          this.employeeService.addEmployee(this.employee).subscribe((res: any) => {
            if (res.Success) {
              Swal.fire('Success', this.commonService.replaceMessage(this.commonService.genericAddSuccessfulMessage, this.commonService.entity.Employee), 'success');
              this.router.navigate(["/"]);
            } else {
              Swal.fire('Error', this.commonService.replaceMessage(this.commonService.genericAddErrorMessage, this.commonService.entity.Employee) + "<br/>" + res.Message, 'error');
            }
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire(
            'Cancelled',
            '',
            'error'
          );
        }
      });
    }
  }
  backToList() {
    this.router.navigate(['']);
  }
  isValidForm() {
    this.formValidation = { FirstName: '', Designation: '', Department: '', DateOfBirth: '', JoiningDate: '' };
    let isValid = true;
    if ((this.employee.FirstName).trim().length === 0) {
      this.formValidation.FirstName = 'First name is required';
      isValid = false;
    }
    if (!this.employee.DesignationId) {
      this.formValidation.Designation = 'Designation is required';
      isValid = false;
    }
    if (!this.employee.DepartmentId) {
      this.formValidation.Department = 'Department is required';
      isValid = false;
    }
    if (!this.employee.DateOfBirth) {
      this.formValidation.DateOfBirth = 'Date Of Birth is required';
      isValid = false;
    }
    if (!this.employee.JoiningDate) {
      this.formValidation.JoiningDate = 'Joining Date is required';
      isValid = false;
    }
    if (this.employee.DateOfBirth && this.employee.JoiningDate) {
      if (this.employee.DateOfBirth > this.employee.JoiningDate) {
        this.formValidation.DateOfBirth = "Date Of Birth can't be greater than Joining date";
        isValid = false;
      }
    }
    return isValid;
  }
}
