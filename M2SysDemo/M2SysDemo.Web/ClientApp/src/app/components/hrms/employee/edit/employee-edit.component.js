"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmployeeEditComponent = void 0;
var EmployeeEditComponent = /** @class */ (function () {
    function EmployeeEditComponent(employeeService, route) {
        var _this = this;
        this.employeeService = employeeService;
        this.route = route;
        this.employee = null;
        this.departmentList = [];
        this.designationList = [];
        this.route.queryParams.subscribe(function (params) {
            _this.employee.Id = parseInt(params.e);
            console.log(_this.employee.Id);
        });
    }
    EmployeeEditComponent.prototype.ngOnInit = function () {
        this.getFilterData();
    };
    EmployeeEditComponent.prototype.getEmployee = function () {
        var _this = this;
        this.employeeService.getEmployee(this.employee.Id).subscribe(function (res) {
            _this.employee = res;
            console.log(_this.employee);
        });
    };
    EmployeeEditComponent.prototype.getFilterData = function () {
        var _this = this;
        this.employeeService.getFilterData().subscribe(function (res) {
            console.log(res);
            if (res != null) {
                if (res.Departments != null) {
                    _this.departmentList = res.Departments;
                }
                if (res.Designations != null) {
                    _this.designationList = res.Designations;
                }
                _this.getEmployee();
            }
        });
    };
    return EmployeeEditComponent;
}());
exports.EmployeeEditComponent = EmployeeEditComponent;
//# sourceMappingURL=employee-edit.component.js.map