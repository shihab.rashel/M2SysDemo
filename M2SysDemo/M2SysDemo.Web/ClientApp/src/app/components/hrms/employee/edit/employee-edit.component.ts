import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Employee } from "../../../../models/hrms/employee.model";
import { M2SysIntFilter } from "../../../../models/m2sys-int-filter";
import { EmployeeService } from "../../../../services/hrms/employee-service";
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { CommonService } from "../../../../services/common/common-service";

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})

export class EmployeeEditComponent implements OnInit {
  employee: Employee;
  departmentList: Array<M2SysIntFilter>;
  designationList: Array<M2SysIntFilter>;
  formValidation: any;
  isViewOnly: boolean;

  ngOnInit(): void {
    this.getFilterData();
  }
  constructor(private employeeService: EmployeeService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private commonService: CommonService) {

    this.employee = new Employee();
    this.departmentList = [];
    this.designationList = [];
    this.formValidation = { FirstName: '', Designation: '', Department: '', DateOfBirth: '', JoiningDate: '' };
    this.isViewOnly = false;

    this.activatedRoute.queryParams.subscribe(params => {
      this.employee.Id = parseInt(params.e);
      this.isViewOnly = params.v == 'true';
    });
  }

  getEmployee() {
    this.employeeService.getEmployee(this.employee.Id).subscribe((res: any) => {
      this.employee = res;
      this.employee.DateOfBirth = new Date(this.employee.DateOfBirth);
      this.employee.JoiningDate = new Date(this.employee.JoiningDate);
      console.log(this.employee);
    });

  }
  getFilterData() {
    this.employeeService.getFilterData().subscribe((res: any) => {
      console.log(res);
      if (res != null) {
        if (res.Departments != null) {
          this.departmentList = res.Departments;
        }

        if (res.Designations != null) {
          this.designationList = res.Designations;
        }
        this.getEmployee();
      }
    });
  }

  editEmployee() {
    if (this.isValidForm()) {
      this.employee.DepartmentId = parseInt(this.employee.DepartmentId.toString());
      this.employee.DesignationId = parseInt(this.employee.DesignationId.toString());
      Swal.fire({
        title: this.commonService.replaceMessage(this.commonService.genericEditConfirmationMessage, this.commonService.entity.Employee),
        text: '',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: this.commonService.genericEditConfirmButtonYesText,
        cancelButtonText: this.commonService.genericEditConfirmButtonNoText
      }).then((result) => {
        if (result.value) {
          this.employeeService.editEmployee(this.employee).subscribe((res: any) => {
            if (res.Success) {
              Swal.fire('Success', this.commonService.replaceMessage(this.commonService.genericEditSuccessfulMessage, this.commonService.entity.Employee) + "<br/>" + res.Message, 'success');
              this.router.navigate(["/"]);
            } else {
              Swal.fire('Error', this.commonService.replaceMessage(this.commonService.genericEditErrorMessage, this.commonService.entity.Employee) + "<br/>" + res.Message, 'error');
            }
          })
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire(
            'Cancelled',
            '',
            'error'
          );
        }
      });
    }
  }

  backToList() {
    this.router.navigate(['']);
  }

  isValidForm() {
    this.formValidation = { FirstName: '', Designation: '', Department: '', DateOfBirth: '', JoiningDate: '' };
    let isValid = true;
    if ((this.employee.FirstName).trim().length === 0) {
      this.formValidation.FirstName = 'First name is required';
      isValid = false;
    }
    if (!this.employee.DesignationId) {
      this.formValidation.Designation = 'Designation is required';
      isValid = false;
    }
    if (!this.employee.DepartmentId) {
      this.formValidation.Department = 'Department is required';
      isValid = false;
    }
    if (!this.employee.DateOfBirth) {
      this.formValidation.DateOfBirth = 'Date Of Birth is required';
      isValid = false;
    }
    if (!this.employee.JoiningDate) {
      this.formValidation.JoiningDate = 'Joining Date is required';
      isValid = false;
    }
    if (this.employee.DateOfBirth && this.employee.JoiningDate) {
      if (this.employee.DateOfBirth > this.employee.JoiningDate) {
        this.formValidation.DateOfBirth = "Date Of Birth can't be greater than Joining date";
        isValid = false;
      }
    }
    return isValid;
  }
}
