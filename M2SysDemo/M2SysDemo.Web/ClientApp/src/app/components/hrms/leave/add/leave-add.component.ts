import { Route } from "@angular/compiler/src/core";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Leave } from "../../../../models/hrms/leave.model";
import { M2SysIntFilter } from "../../../../models/m2sys-int-filter";
import { EmployeeLeaveService } from "../../../../services/hrms/emplyoee-leave-service";
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { CommonService } from "../../../../services/common/common-service";

@Component({
  selector: 'app-leave-add',
  templateUrl: './leave-add.component.html',
  styleUrls: ['./leave-add.component.css']
})
export class EmployeeLeaveAddComponent implements OnInit {
  leave: Leave;
  formValidation: any;
  leaveTypes: Array<M2SysIntFilter>;
  ngOnInit(): void {
    this.getFilterData();
  }
  constructor(private employeeLeaveService: EmployeeLeaveService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private commonService: CommonService) {

    this.leave = new Leave();
    this.formValidation = { StartDate: '', EndDate: '', LeaveType: '' };
    this.leaveTypes = [];

    this.activatedRoute.queryParams.subscribe(params => {
      console.log(params);
      this.leave.EmployeeId = parseInt(params.e);
    });
  }

  getFilterData() {
    this.employeeLeaveService.getFilterData().subscribe((res: any) => {
      console.log(res);
      if (res != null) {
        if (res.LeaveTypes != null) {
          this.leaveTypes = res.LeaveTypes;
          this.leave.LeaveType = this.leaveTypes[0].Id;
        }
      }
    });
  }

  addEmployeeLeave() {
    if (this.isValidForm()) {
      this.leave.LeaveType = parseInt(this.leave.LeaveType.toString());
      Swal.fire({
        title: this.commonService.replaceMessage(this.commonService.genericAddConfirmationMessage, this.commonService.entity.Leave),
        text: '',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: this.commonService.genericAddConfirmButtonYesText,
        cancelButtonText: this.commonService.genericAddConfirmButtonNoText
      }).then((result) => {
        if (result.value) {
          this.employeeLeaveService.addEmployeeLeave(this.leave).subscribe((res: any) => {
            if (res.Success) {
              Swal.fire('Success', this.commonService.replaceMessage(this.commonService.genericAddSuccessfulMessage, this.commonService.entity.Leave), 'success');
              this.router.navigate(['leave-list'], { queryParams: { e: this.leave.EmployeeId } });
            } else {
              Swal.fire('Error', this.commonService.replaceMessage(this.commonService.genericAddErrorMessage, this.commonService.entity.Leave) + "<br/>" + res.Message, 'error');
            }
          });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire(
            'Cancelled',
            '',
            'error'
          );
        }
      });
    }
  }

  backToList() {
    this.router.navigate(['leave-list']);
  }

  isValidForm() {
    this.formValidation = { StartDate: '', EndDate: '', LeaveType: '' }
    var isValidForm = true;
    if (!this.leave.StartDate) {
      this.formValidation.StartDate = 'Start Date is required';
      isValidForm = false;
    }
    if (!this.leave.EndDate) {
      this.formValidation.EndDate = 'End Date is required';
      isValidForm = false;
    }
    if (!this.leave.LeaveType) {
      this.formValidation.LeaveType = 'Leave Type is required';
      isValidForm = false;
    }
    if (this.leave.StartDate && this.leave.EndDate) {
      const tempStartDate = new Date(this.leave.StartDate.getFullYear(),
        this.leave.StartDate.getMonth(),
        this.leave.StartDate.getDate());
      const tempEndDate = new Date(this.leave.EndDate.getFullYear(),
        this.leave.EndDate.getMonth(),
        this.leave.EndDate.getDate());
      if (tempStartDate > tempEndDate) {
        this.formValidation.StartDate = "Start date can't be greater than end date";
        isValidForm = false;
      }
    }
    return isValidForm;
  }
}
