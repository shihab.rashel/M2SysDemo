import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Leave } from "../../../../models/hrms/leave.model";
import { M2SysIntFilter } from "../../../../models/m2sys-int-filter";
import { EmployeeService } from "../../../../services/hrms/employee-service";
import { EmployeeLeaveService } from "../../../../services/hrms/emplyoee-leave-service";
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { CommonService } from "../../../../services/common/common-service";

@Component({
  selector: 'app-leave-list',
  templateUrl: './leave-list.component.html',
  styleUrls: ['./leave-list.component.css']
})
export class EmployeeLeaveListComponent implements OnInit {
  employeeLeaveList: Array<Leave>;
  employeeId: number;
  employees: Array<M2SysIntFilter>;
  searchParam: string = "";
  rowNumberListToShow: Array<number>;
  rowNumberToShow: number;
  pageNumber: number;
  emplyeeLeaveCount: number = 0;
  totalPage: number = 0;

  ngOnInit(): void {
    this.getEmployees();
  }

  constructor(private employeeLeaveService: EmployeeLeaveService,
    private employeeService: EmployeeService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private commonService: CommonService) {

    this.employeeId = 0;
    this.employeeLeaveList = [];
    this.employees = [];
    this.searchParam = "";
    this.rowNumberListToShow = [];
    this.rowNumberListToShow.push(2);
    this.rowNumberListToShow.push(10);
    this.rowNumberListToShow.push(20);
    this.rowNumberListToShow.push(50);
    this.rowNumberToShow = this.rowNumberListToShow[0];
    this.pageNumber = 1;
    this.emplyeeLeaveCount = 0;
    this.totalPage = 0;

    this.activatedRoute.queryParams.subscribe(params => {
      if (params.e)
        this.employeeId = parseInt(params.e);
    });
  }

  getEmployeeLeaveCount() {
    this.pageNumber = 1;
    this.employeeLeaveList = [];
    this.employeeLeaveService.getEmployeeLeaveCount(this.employeeId, this.searchParam).subscribe((res: any) => {
      this.emplyeeLeaveCount = res;
      this.totalPage = Math.ceil(this.emplyeeLeaveCount / this.rowNumberToShow);
      this.getLeaveListByEmployee();
    });
  }

  getEmployees() {
    this.employeeService.getEmployeeFilter().subscribe((res: any) => {
      console.log(res);
      if (res != null) {
        this.employees = res;
        console.log(this.employees);
        this.employeeId = this.employeeId == 0 ? this.employees[0].Id : this.employeeId;
        this.getEmployeeLeaveCount();
      }
    });
  }

  getLeaveListByEmployee() {
    this.employeeLeaveService
      .getLeavesByEmployee(this.employeeId, this.pageNumber, this.rowNumberToShow, this.searchParam).subscribe(
        (res: any) => {
          this.employeeLeaveList = res;
        });
  }

  navigateAddEmployeeLeave() {
    this.router.navigate(['leave-add'], { queryParams: { e: this.employeeId } });
  }

  navigateEditEmployeeLeave(leaveId) {
    this.router.navigate(['leave-edit'], { queryParams: { e: this.employeeId, l: leaveId } });
  }

  onPreviousClick() {
    if (this.pageNumber > 1)
      this.pageNumber -= 1;
    this.getLeaveListByEmployee();
  }

  onNextClick() {
    this.pageNumber++;
    if (this.pageNumber > this.totalPage)
      this.pageNumber = this.totalPage;
    this.getLeaveListByEmployee();
  }

  deleteEmployeeLeave(id: number) {
    Swal.fire({
      title: this.commonService.replaceMessage(this.commonService.genericDeleteConfirmationMessage, this.commonService.entity.Leave),
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: this.commonService.genericDeleteConfirmButtonYesText,
      cancelButtonText: this.commonService.genericDeleteConfirmButtonNoText
    }).then((result) => {
      if (result.value) {
        this.employeeLeaveService.deleteEmployeeLeave(id).subscribe((res: any) => {
          if (res.Success) {
            Swal.fire('Success', this.commonService.replaceMessage(this.commonService.genericDeleteSuccessfulMessage, this.commonService.entity.Leave), 'success');
            this.employeeLeaveList = [];
            this.getEmployeeLeaveCount();
          } else {
            Swal.fire('Error', this.commonService.replaceMessage(this.commonService.genericDeleteErrorMessage, this.commonService.entity.Leave), 'error');
          }
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          '',
          'error'
        );
      }
    });
  }
}
