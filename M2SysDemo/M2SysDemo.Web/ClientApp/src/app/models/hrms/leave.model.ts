export class Leave {
  Id: number;
  EmployeeId: number;
  LeaveType: number;
  LeaveTypeName: string;
  Description: string;
  StartDate: Date;
  EndDate: Date;
  constructor() {
    this.Id = 0;
    this.EmployeeId = -1;
    this.LeaveType = -1;
    this.LeaveTypeName = '';
    this.Description = '';
    this.StartDate = new Date();
    this.EndDate = new Date();
  }
}
