export class Employee {
  Id: number;
  FirstName: string;
  MiddleName: string;
  LastName: string;
  DateOfBirth: Date;
  JoiningDate: Date;
  DesignationId: number;
  DepartmentId: number;
  DepartmentName: string;
  DesignationName: string;
  constructor() {
    this.Id = 0;
    this.FirstName = '';
    this.MiddleName = '';
    this.JoiningDate = new Date();
    this.DateOfBirth = new Date();
    this.DesignationId = -1;
    this.DepartmentId = -1;
    this.DesignationName = '';
    this.DepartmentName = '';
  }
}

