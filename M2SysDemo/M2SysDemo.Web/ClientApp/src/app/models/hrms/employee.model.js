"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Employee = void 0;
var Employee = /** @class */ (function () {
    function Employee() {
        this.Id = 0;
        this.FirstName = '';
        this.MiddleName = '';
        this.JoiningDate = new Date();
        this.DateOfBirth = new Date();
        this.DesignationId = -1;
        this.DepartmentId = -1;
        this.DesignationName = '';
        this.DepartmentName = '';
    }
    return Employee;
}());
exports.Employee = Employee;
//# sourceMappingURL=employee.model.js.map