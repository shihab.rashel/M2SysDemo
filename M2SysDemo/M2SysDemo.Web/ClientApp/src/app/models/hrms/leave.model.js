"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Leave = void 0;
var Leave = /** @class */ (function () {
    function Leave() {
        this.Id = 0;
        this.EmployeeId = -1;
        this.LeaveType = -1;
        this.LeaveTypeName = '';
        this.Description = '';
        this.StartDate = new Date();
        this.EndDate = new Date();
    }
    return Leave;
}());
exports.Leave = Leave;
//# sourceMappingURL=leave.model.js.map