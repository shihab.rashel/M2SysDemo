import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { EmployeeListComponet } from './components/hrms/employee/list/employee-list.component';
import { EmployeeService } from './services/hrms/employee-service';
import { EmployeeAddComponent } from './components/hrms/employee/add/employee-add.component';
import { EmployeeEditComponent } from './components/hrms/employee/edit/employee-edit.component';
import { EmployeeLeaveListComponent } from './components/hrms/leave/list/leave-list.component';
import { EmployeeLeaveAddComponent } from './components/hrms/leave/add/leave-add.component';
import { EmployeeLeaveEditComponent } from './components/hrms/leave/edit/leave-edit.component';
import { EmployeeLeaveService } from './services/hrms/emplyoee-leave-service';
import { CommonService } from "./services/common/common-service";

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    EmployeeListComponet,
    EmployeeAddComponent,
    EmployeeEditComponent,
    EmployeeLeaveListComponent,
    EmployeeLeaveAddComponent,
    EmployeeLeaveEditComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: EmployeeListComponet, pathMatch: 'full' },
      { path: 'employee-add', component: EmployeeAddComponent, pathMatch: 'full' },
      { path: 'employee-edit', component: EmployeeEditComponent, pathMatch: 'full' },
      { path: 'leave-list', component: EmployeeLeaveListComponent, pathMatch: 'full'},
      { path: 'leave-add', component: EmployeeLeaveAddComponent, pathMatch: 'full' },
      { path: 'leave-edit', component: EmployeeLeaveEditComponent, pathMatch: 'full' }    
    ])
  ],
  providers: [
    EmployeeService,
    EmployeeLeaveService,
    CommonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
