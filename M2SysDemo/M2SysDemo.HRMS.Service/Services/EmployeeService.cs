﻿using AutoMapper;
using M2SysDemo.AppModel;
using M2SysDemo.AppModel.HRMS;
using M2SysDemo.DAL.Models;
using M2SysDemo.DAL.Repository.HRMS.Interfaces;
using M2SysDemo.DAL.Repository.HRMS.Repositories;
using M2SysDemo.HRMS.Service.Interfaces;
using M2SysDemo.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M2SysDemo.HRMS.Service.Services
{
    public partial class EmployeeService : GenericCrudService, IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeLeaveRepository _employeeLeaveRepository;
        private readonly MapperConfiguration _config;
        public EmployeeService(string connectionString) : base(connectionString)
        {
            _employeeRepository = new EmployeeRepository(connectionString);
            _employeeLeaveRepository=new EmployeeLeaveRepository(connectionString);
            _config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Employee, AppEmployee>();
                cfg.CreateMap<AppEmployee, Employee>();
            });
        }
        public virtual AppEmployee Get(int employeeId)
        {
            try
            {
                var dbEmployee = Get<Employee>(employeeId);
                if (dbEmployee == null) return null;
                var mapper = _config.CreateMapper();
                return mapper.Map<Employee, AppEmployee>(dbEmployee);

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public virtual List<AppEmployee> GetAll(int pageNumber, int rowsNumberPerPage, string searchParam)
        {
            try
            {
                searchParam = string.IsNullOrEmpty(searchParam) ? string.Empty : searchParam;
                var dbEmployeeList = _employeeRepository.GetAll(pageNumber, rowsNumberPerPage, searchParam);
                if (dbEmployeeList == null || !dbEmployeeList.Any()) return null;
                var mapper = _config.CreateMapper();
                var appEmployeeList = dbEmployeeList.Select(e => mapper.Map<Employee, AppEmployee>(e)).ToList();
                var designationList = GetAll<Designation>();
                var departmentList = GetAll<Department>();
                foreach (var employee in appEmployeeList)
                {
                    var department = departmentList.FirstOrDefault(dept => dept.Id == employee.DepartmentId);
                    employee.DepartmentName = department == null ? "" : department.Name;

                    var designation = designationList.FirstOrDefault(desg => desg.Id == employee.DesignationId);
                    employee.DesignationName = designation == null ? "" : designation.Name;
                }
                return appEmployeeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public virtual List<AppEmployee> GetAllForSearch(string searchParam)
        {
            try
            {
                var dbEmployeeList = _employeeRepository.GetAllForSearch(searchParam);
                if (dbEmployeeList == null || !dbEmployeeList.Any()) return null;
                var mapper = _config.CreateMapper();
                var appEmployeeList = dbEmployeeList.Select(e => mapper.Map<Employee, AppEmployee>(e)).ToList();
                var designationList = GetAll<Designation>();
                var departmentList = GetAll<Department>();
                foreach (var employee in appEmployeeList)
                {
                    var department = departmentList.FirstOrDefault(dept => dept.Id == employee.DepartmentId);
                    employee.DepartmentName = department == null ? "" : department.Name;

                    var designation = designationList.FirstOrDefault(desg => desg.Id == employee.DesignationId);
                    employee.DepartmentName = designation == null ? "" : designation.Name;
                }
                return appEmployeeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public virtual CrudResult Add(AppEmployee employee)
        {
            try
            {
                var mapper = _config.CreateMapper();
                var dbEmployee = mapper.Map<AppEmployee, Employee>(employee);
                return Add(dbEmployee);
            }
            catch (Exception ex)
            {
                return new CrudResult(false, "");
            }
        }
        public virtual CrudResult Edit(AppEmployee employee)
        {
            try
            {
                var mapper = _config.CreateMapper();
                var dbEmployee = mapper.Map<AppEmployee, Employee>(employee);
                return Edit(dbEmployee);
            }
            catch (Exception ex)
            {
                return new CrudResult(false, "");
            }
        }
        public virtual CrudResult Delete(int employeeId)
        {
            try
            {
                var allLeaveOfThisEmployee = _employeeLeaveRepository.GetLeavesByEmployee(employeeId);
                if(!allLeaveOfThisEmployee.Any())
                    return Delete<Employee>(employeeId);
                foreach (var employeeLeave in allLeaveOfThisEmployee)
                {
                    if(!Delete<Leave>(employeeLeave.Id).Success)
                        return new CrudResult(false,"");
                }
                return Delete<Employee>(employeeId);
            }
            catch (Exception ex)
            {
                return new CrudResult(false, "");
            }
        }

        public virtual Dictionary<string, List<M2SysIntFilter>> GetFilterData()
        {
            try
            {
                var filters = new Dictionary<string, List<M2SysIntFilter>>();
                var departments = GetAll<Department>();
                if (departments != null && departments.Any())
                {
                    filters.Add("Departments",
                        departments.Select(d => new M2SysIntFilter {Id = d.Id, Name = d.Name}).ToList());
                }

                var designations = GetAll<Designation>();
                if (designations != null && designations.Any())
                {
                    filters.Add("Designations",
                        designations.Select(d => new M2SysIntFilter {Id = d.Id, Name = d.Name}).ToList());
                }
                return filters;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public virtual int GetTotalEmployeeCount(string searchParam)
        {
            return _employeeRepository.GetTotalEmployeeCount(searchParam);
        }

        public virtual List<M2SysIntFilter> GetEmployeeFilterData()
        {
            var employees = GetAll<Employee>();
            if (employees == null || !employees.Any()) return null;
            return employees.Select(emp => new M2SysIntFilter
            {
                Id = emp.Id,
                Name = emp.FirstName + (string.IsNullOrEmpty(emp.MiddleName) ? "" : " ") + emp.MiddleName +
                       (string.IsNullOrEmpty(emp.LastName) ? "" : " ") + emp.LastName
            }).ToList();
        }
    }
}
