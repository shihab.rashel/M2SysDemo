﻿using AutoMapper;
using M2SysDemo.AppModel;
using M2SysDemo.AppModel.HRMS;
using M2SysDemo.DAL.Models;
using M2SysDemo.DAL.Repository.HRMS.Interfaces;
using M2SysDemo.DAL.Repository.HRMS.Repositories;
using M2SysDemo.HRMS.Service.Interfaces;
using M2SysDemo.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M2SysDemo.HRMS.Service.Services
{
    public partial class EmployeeLeaveService : GenericCrudService, IEmployeeLeaveService
    {
        private readonly IEmployeeLeaveRepository _employeeLeaveRepository;
        private readonly MapperConfiguration _config;
        public EmployeeLeaveService(string connectionString) : base(connectionString)
        {
            _employeeLeaveRepository = new EmployeeLeaveRepository(connectionString);
            _config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Leave, AppLeave>();
                cfg.CreateMap<AppLeave, Leave>();
            });
        }
        public virtual AppLeave Get(int id)
        {
            try
            {
                var dbLeave = Get<Leave>(id);
                if (dbLeave == null) return null;
                var mapper = _config.CreateMapper();
                return mapper.Map<Leave, AppLeave>(dbLeave);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public virtual List<AppLeave> GetLeavesByEmployee(int employeeId, int pageNumber, int rowsNumberPerPage, string searchParam)
        {
            try
            {
                var employeeLeaveList = _employeeLeaveRepository.GetLeavesByEmployee(employeeId, pageNumber, rowsNumberPerPage, string.IsNullOrEmpty(searchParam) ? string.Empty : searchParam);
                if (employeeLeaveList == null || !employeeLeaveList.Any()) return null;
                var mapper = _config.CreateMapper();
                var appEmployeeList = employeeLeaveList.Select(l => mapper.Map<Leave, AppLeave>(l)).ToList();
                var leaveTypes = GetByProperty<OrganizationalConfig>(oc => oc.Type.Equals("LeaveType"));
                if (leaveTypes == null || !leaveTypes.Any()) return appEmployeeList;
                foreach (var appEmployee in appEmployeeList)
                {
                    var leaveType = leaveTypes.FirstOrDefault(lt => lt.Value.Equals(appEmployee.LeaveType.ToString()));
                    appEmployee.LeaveTypeName = leaveType == null ? string.Empty : leaveType.Name;
                }
                return appEmployeeList;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public virtual CrudResult Add(AppLeave leave)
        {
            try
            {
                var isValid = IsLeaveValid(leave,false);
                if (!isValid.Success)
                    return isValid;
                var mapper = _config.CreateMapper();
                var dbLeave = mapper.Map<AppLeave, Leave>(leave);
                return Add(dbLeave);
            }
            catch (Exception ex)
            {
                return new CrudResult(false, "");
            }
        }

        public virtual CrudResult Edit(AppLeave leave)
        {
            try
            {
                var isValid = IsLeaveValid(leave,true);
                if (!isValid.Success)
                    return isValid;
                var mapper = _config.CreateMapper();                
                var dbLeave = mapper.Map<AppLeave, Leave>(leave);
                return Edit(dbLeave);
            }
            catch (Exception ex)
            {
                return new CrudResult(false, "");
            }
        }
        public virtual CrudResult Delete(int id)
        {
            try
            {
                return Delete<Leave>(id);
            }
            catch (Exception ex)
            {
                return new CrudResult(false, "");
            }
        }

        public virtual Dictionary<string, List<M2SysIntFilter>> GetFilterData()
        {
            try
            {
                var filters = new Dictionary<string, List<M2SysIntFilter>>();
                var leaveTypes = GetByProperty<OrganizationalConfig>(c => c.Type.Equals("LeaveType"));
                if (leaveTypes != null && leaveTypes.Any())
                {
                    filters.Add("LeaveTypes",
                        leaveTypes.Select(d => new M2SysIntFilter {Id = int.Parse(d.Value), Name = d.Name}).ToList());
                }
                return filters;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public virtual int GetEmployeeLeaveCount(int employeeId, string searchParam)
        {
            try
            {
                return _employeeLeaveRepository.GetEmployeeLeaveCount(employeeId,
                    string.IsNullOrEmpty(searchParam) ? string.Empty : searchParam);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        private CrudResult IsLeaveValid(AppLeave appLeave,bool isEdit)
        {
            var allLeaveOfTheEmployeeBetweenThisStartDateAndEndDate = isEdit
                ? _employeeLeaveRepository.GetEmployeeLeaveByDateRange(appLeave.EmployeeId, appLeave.StartDate,
                    appLeave.EndDate, appLeave.Id)
                : _employeeLeaveRepository.GetEmployeeLeaveByDateRange(appLeave.EmployeeId, appLeave.StartDate,
                    appLeave.EndDate);
            return allLeaveOfTheEmployeeBetweenThisStartDateAndEndDate.Any()
                ? new CrudResult(false,
                    "Employee already has leave between the selected date. Please select start date after " +
                    allLeaveOfTheEmployeeBetweenThisStartDateAndEndDate.Last().EndDate.ToShortDateString())
                : new CrudResult(true, "");
        }
    }
}
