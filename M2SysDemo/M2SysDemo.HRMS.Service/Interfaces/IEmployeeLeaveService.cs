﻿using M2SysDemo.AppModel;
using M2SysDemo.AppModel.HRMS;
using System;
using System.Collections.Generic;
using System.Text;

namespace M2SysDemo.HRMS.Service.Interfaces
{
    public partial interface IEmployeeLeaveService
    {
        AppLeave Get(int id);
        List<AppLeave> GetLeavesByEmployee(int employeeId, int pageNumber, int rowsNumberPerPage, string searchParam);
        CrudResult Add(AppLeave leave);
        CrudResult Edit(AppLeave leave);
        CrudResult Delete(int id);
        Dictionary<string, List<M2SysIntFilter>> GetFilterData();
        int GetEmployeeLeaveCount(int employeeId, string searchParam);
    }
}
