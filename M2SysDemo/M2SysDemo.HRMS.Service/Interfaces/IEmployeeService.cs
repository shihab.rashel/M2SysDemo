﻿using M2SysDemo.AppModel;
using M2SysDemo.AppModel.HRMS;
using System;
using System.Collections.Generic;
using System.Text;

namespace M2SysDemo.HRMS.Service.Interfaces
{
    public partial interface IEmployeeService
    {
        AppEmployee Get(int employeeId);
        List<AppEmployee> GetAll(int pageNumber, int rowsNumberPerPage, string searchParam);
        List<AppEmployee> GetAllForSearch(string searchParam);
        CrudResult Add(AppEmployee employee);
        CrudResult Edit(AppEmployee employee);
        CrudResult Delete(int employeeId);
        Dictionary<string, List<M2SysIntFilter>> GetFilterData();
        int GetTotalEmployeeCount(string searchParam);
        List<M2SysIntFilter> GetEmployeeFilterData();
    }
}
