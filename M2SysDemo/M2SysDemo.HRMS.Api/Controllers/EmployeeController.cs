﻿using System.Collections.Generic;
using M2SysDemo.AppModel;
using M2SysDemo.AppModel.HRMS;
using M2SysDemo.HRMS.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace M2SysDemo.HRMS.Api.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    [Route("api/v{version:apiVersion}/employee")]

    public partial class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }
        [HttpGet("filterdata")]
        public virtual ActionResult<Dictionary<string, List<M2SysIntFilter>>> GetFilterData()
        {
            var data = _employeeService.GetFilterData();
            if(data!=null)
                return Ok(data);
            return NotFound();
        }
        [HttpGet("employeecount/{searchParam?}")]
        public virtual ActionResult<int> GetTotalEmployeeCount(string searchParam)
        {
            var employeeCount = _employeeService.GetTotalEmployeeCount(searchParam);
            return Ok(employeeCount);
        }
        [HttpGet("{pageNumber}/{rowsNumberPerPage}/{searchParam?}")]
        public virtual ActionResult<IEnumerable<AppEmployee>> GetAll(int pageNumber, int rowsNumberPerPage, string searchParam)
        {
            var employees = _employeeService.GetAll(pageNumber, rowsNumberPerPage, searchParam);
            if (employees != null)
                return Ok(employees);
            return NotFound();
        }
        //[HttpGet("search/{searchParam}")]
        //public virtual ActionResult<IEnumerable<AppEmployee>> GetAllForSearch(string searchParam)
        //{
        //    var employees = _employeeService.GetAllForSearch(searchParam);
        //    if(employees!=null)
        //        return Ok(employees);
        //    return NotFound();
        //}
        [HttpGet("{employeeId}")]
        public virtual ActionResult<AppEmployee> Get(int employeeId)
        {
            var employee = _employeeService.Get(employeeId);
            if(employee!=null)
                return Ok(employee);
            return NotFound();
        }
        [HttpGet("employeefilter")]
        public virtual ActionResult<IEnumerable<M2SysIntFilter>> GetEmployeeFilterData()
        {
            var employees = _employeeService.GetEmployeeFilterData();
            if(employees!=null)
                return Ok(employees);
            return NotFound();
        }

        [HttpPost]
        public virtual ActionResult<CrudResult> Add(AppEmployee employee)
        {
            var result = _employeeService.Add(employee);
            return Ok(result);
        }
        [HttpPut]
        public virtual ActionResult<CrudResult> Edit(AppEmployee employee)
        {
            var result = _employeeService.Edit(employee);
            return Ok(result);
        }
        [HttpDelete("{employeeId}")]
        public virtual ActionResult<CrudResult> Delete(int employeeId)
        {
            var result = _employeeService.Delete(employeeId);
            return Ok(result);
        }
    }
}
