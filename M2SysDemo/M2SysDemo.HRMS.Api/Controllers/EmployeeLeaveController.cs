﻿using System.Collections.Generic;
using M2SysDemo.AppModel;
using M2SysDemo.AppModel.HRMS;
using M2SysDemo.HRMS.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace M2SysDemo.HRMS.Api.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/leave")]
    public partial class EmployeeLeaveController : ControllerBase
    {
        private readonly IEmployeeLeaveService _employeeLeaveService;
        public EmployeeLeaveController(IEmployeeLeaveService employeeLeaveService)
        {
            _employeeLeaveService = employeeLeaveService;
        }
        [HttpGet("{id}")]
        public virtual ActionResult<AppLeave> Get(int id)
        {
            var appLeave = _employeeLeaveService.Get(id);
            if (appLeave != null)
                return Ok(appLeave);
            return NotFound();
        }
        [HttpGet("employeeleavecount/{employeeId}/{searchParam?}")]
        public virtual ActionResult<int> GetEmployeeLeaveCount(int employeeId, string searchParam)
        {
            var employeeLeaveCount = _employeeLeaveService.GetEmployeeLeaveCount(employeeId, searchParam);
            return Ok(employeeLeaveCount);
        }
        [HttpGet("employeeleave/{employeeId}/{pageNumber}/{rowsNumberPerPage}/{searchParam?}")]
        public virtual ActionResult<IEnumerable<AppLeave>> GetLeavesByEmployee(int employeeId, int pageNumber, int rowsNumberPerPage, string searchParam)
        {
            var leaveList = _employeeLeaveService.GetLeavesByEmployee(employeeId, pageNumber, rowsNumberPerPage, searchParam);
            if(leaveList!=null)
                return Ok(leaveList);
            return NotFound();
        }
        [HttpGet("filterdata")]
        public virtual ActionResult<Dictionary<string, List<M2SysIntFilter>>> GetFilterData()
        {
            var data = _employeeLeaveService.GetFilterData();
            if(data!=null)
                return Ok(data);
            return NotFound();
        }
        [HttpPost]
        public virtual ActionResult<CrudResult> Add(AppLeave leave)
        {
            var result = _employeeLeaveService.Add(leave);
            return Ok(result);
        }
        [HttpPut]
        public virtual ActionResult<CrudResult> Edit(AppLeave leave)
        {
            var result = _employeeLeaveService.Edit(leave);
            return Ok(result);
        }
        [HttpDelete("{id}")]
        public virtual ActionResult<CrudResult> Delete(int id)
        {
            var result = _employeeLeaveService.Delete(id);
            return Ok(result);
        }
    }
}
