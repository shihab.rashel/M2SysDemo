﻿using M2SysDemo.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace M2SysDemo.DAL.Repository.HRMS.Interfaces
{
    public partial interface IEmployeeLeaveRepository
    {
        List<Leave> GetLeavesByEmployee(int employeeId, int pageNumber, int rowsNumberPerPage, string searchParam);
        List<Leave> GetLeavesByEmployee(int employeeId);
        int GetEmployeeLeaveCount(int employeeId, string searchParam);
        List<Leave> GetEmployeeLeaveByDateRange(int employeeId, DateTime startDate, DateTime endDate);
        List<Leave> GetEmployeeLeaveByDateRange(int employeeId, DateTime startDate, DateTime endDate, int leavedId);
    }
}
