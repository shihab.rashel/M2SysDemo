﻿using M2SysDemo.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace M2SysDemo.DAL.Repository.HRMS.Interfaces
{
    public partial interface IEmployeeRepository
    {
        List<Employee> GetAll(int pageNumber, int rowsNumberPerPage, string searchParam);
        List<Employee> GetAllForSearch(string searchParam);
        int GetTotalEmployeeCount(string searchParam);
    }
}
