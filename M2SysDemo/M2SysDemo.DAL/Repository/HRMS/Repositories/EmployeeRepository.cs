﻿using M2SysDemo.DAL.Models;
using M2SysDemo.DAL.Repository.HRMS.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M2SysDemo.DAL.Repository.HRMS.Repositories
{
    public partial class EmployeeRepository : IEmployeeRepository
    {
        private readonly DbContextOptionsBuilder<M2SysDemoDbContext> _dbContextOptionBuilder;
        public EmployeeRepository(string connectionString)
        {
            _dbContextOptionBuilder = new DbContextOptionsBuilder<M2SysDemoDbContext>();
            _dbContextOptionBuilder.UseSqlServer(connectionString);
        }

        public virtual List<Employee> GetAll(int pageNumber, int rowsNumberPerPage, string searchParam)
        {
            try
            {
                using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
                var employeeList = context.Employee
                    .FromSqlRaw("EXECUTE dbo.GetEmployeeList {0},{1},{2}", searchParam, pageNumber, rowsNumberPerPage)
                    .ToList();
                return employeeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public virtual List<Employee> GetAllForSearch(string searchParam)
        {
            try
            {
                using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
                var employeeList = context.Employee
                    .FromSqlRaw("EXECUTE dbo.GetEmployeeFromSearch {0}", searchParam)
                    .ToList();
                return employeeList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public virtual int GetTotalEmployeeCount(string searchParam)
        {
            try
            {
                using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
                return string.IsNullOrEmpty(searchParam)
                    ? context.Employee.Count()
                    : (from emp in context.Employee
                        join dept in context.Department on emp.DepartmentId equals dept.Id
                        join desg in context.Designation on emp.DesignationId equals desg.Id
                        where string.IsNullOrEmpty(searchParam) ||
                              emp.FirstName.Contains(searchParam) ||
                              emp.MiddleName.Contains(searchParam) ||
                              emp.LastName.Contains(searchParam) ||
                              emp.DateOfBirth.ToString().Contains(searchParam) ||
                              emp.JoiningDate.ToString().Contains(searchParam) ||
                              dept.Name.Contains(searchParam) ||
                              desg.Name.Contains(searchParam)
                        select emp).Count();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
