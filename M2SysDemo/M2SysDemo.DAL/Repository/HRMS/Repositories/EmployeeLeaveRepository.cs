﻿using M2SysDemo.DAL.Models;
using M2SysDemo.DAL.Repository.HRMS.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M2SysDemo.DAL.Repository.HRMS.Repositories
{
    public partial class EmployeeLeaveRepository : IEmployeeLeaveRepository
    {
        private readonly DbContextOptionsBuilder<M2SysDemoDbContext> _dbContextOptionBuilder;
        public EmployeeLeaveRepository(string connectionString)
        {
            _dbContextOptionBuilder = new DbContextOptionsBuilder<M2SysDemoDbContext>();
            _dbContextOptionBuilder.UseSqlServer(connectionString);
        }
        public virtual List<Leave> GetEmployeeLeaveByDateRange(int employeeId, DateTime startDate, DateTime endDate)
        {
            using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
            return context.Leave
                .Where(l =>
                    l.EmployeeId == employeeId &&
                    l.StartDate.Date <= startDate.Date &&
                    l.EndDate.Date >= startDate.Date)
                .ToList();
        }
        public virtual List<Leave> GetEmployeeLeaveByDateRange(int employeeId, DateTime startDate, DateTime endDate, int leaveId)
        {
            using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
            return
                context.Leave
                    .Where(l =>
                        l.Id != leaveId &&
                        l.EmployeeId == employeeId &&
                        l.StartDate.Date <= startDate.Date &&
                        l.EndDate.Date >= startDate.Date)
                    .ToList();
        }

        public virtual int GetEmployeeLeaveCount(int employeeId, string searchParam)
        {
            try
            {
                using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
                return (
                    from leave in context.Leave
                    join leaveType in context.OrganizationalConfig on leave.LeaveType.ToString() equals leaveType.Value
                    where leave.EmployeeId == employeeId &&
                          leaveType.Type.Equals("LeaveType") &&
                          (
                              string.IsNullOrEmpty(searchParam) ||
                              leave.Description.Contains(searchParam) ||
                              leave.StartDate.ToString().Contains(searchParam) ||
                              leave.EndDate.ToString().Contains(searchParam) ||
                              leaveType.Name.ToString().Contains(searchParam)
                          )
                    select leave
                ).Count();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public virtual List<Leave> GetLeavesByEmployee(int employeeId, int pageNumber, int rowsNumberPerPage, string searchParam)
        {
            try
            {
                using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
                return context.Leave
                    .FromSqlRaw("EXECUTE dbo.GetEmployeeLeaveList {0},{1},{2},{3}", employeeId, searchParam, pageNumber,
                        rowsNumberPerPage)
                    .ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public virtual List<Leave> GetLeavesByEmployee(int employeeId)
        {
            try
            {
                using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
                return context.Leave.Where(l=>l.EmployeeId==employeeId).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
