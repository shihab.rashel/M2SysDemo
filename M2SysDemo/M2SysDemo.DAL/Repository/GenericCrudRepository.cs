﻿using M2SysDemo.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace M2SysDemo.DAL.Repository
{
    public partial class GenericCrudRepository: IGenericCrudRepository
    {
        private readonly DbContextOptionsBuilder<M2SysDemoDbContext> _dbContextOptionBuilder;
        public GenericCrudRepository(string connectionString)
        {
            _dbContextOptionBuilder = new DbContextOptionsBuilder<M2SysDemoDbContext>();
            _dbContextOptionBuilder.UseSqlServer(connectionString);
        }

        public virtual T Get<T>(int id) where T : class
        {
            using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
            var dbSet = context.Set<T>();
            var entity = dbSet.Find(id);
            return entity;
        }

        public virtual List<T> GetAll<T>() where T : class
        {
            using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
            var dbSet = context.Set<T>();
            var entityList = dbSet.AsNoTracking().ToList();
            return entityList;
        }
        public virtual List<T> GetByProperty<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
            var dbSet = context.Set<T>();
            var entityList = dbSet.Where(predicate).ToList();
            return entityList;
        }
        public virtual T Add<T>(T entity) where T : class
        {
            using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
            var dbSet = context.Set<T>();
            dbSet.Add(entity);
            context.SaveChanges();
            return entity;
        }
        public virtual T Edit<T>(T entity) where T : class
        {
            using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
            return entity;
        }
        public virtual T Delete<T>(int id) where T : class
        {
            using var context = new M2SysDemoDbContext(_dbContextOptionBuilder.Options);
            var dbSet = context.Set<T>();
            var entity = dbSet.Find(id);
            dbSet.Remove(entity);
            context.SaveChanges();
            return entity;
        }

    }
}
