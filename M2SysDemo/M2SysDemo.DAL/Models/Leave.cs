﻿using System;
using System.Collections.Generic;

namespace M2SysDemo.DAL.Models
{
    public partial class Leave
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int LeaveType { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
