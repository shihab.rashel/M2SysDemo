﻿using System;
using System.Collections.Generic;

namespace M2SysDemo.DAL.Models
{
    public partial class OrganizationalConfig
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool Status { get; set; }
        public int Order { get; set; }
    }
}
