﻿using System;
using System.Collections.Generic;

namespace M2SysDemo.DAL.Models
{
    public partial class Employee
    {
        public Employee()
        {
            Leave = new HashSet<Leave>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime JoiningDate { get; set; }
        public int DesignationId { get; set; }
        public int DepartmentId { get; set; }

        public virtual Department Department { get; set; }
        public virtual Designation Designation { get; set; }
        public virtual ICollection<Leave> Leave { get; set; }
    }
}
